from .interface_belt_manager import InterfaceBeltManager
from .furniture import Furniture
from .dragon_ball import DragonBall, DBHeroes
from .gift_wrap import GiftWrap
from .box import Box
import random


class BeltManager(InterfaceBeltManager):

    instance = None
    count = 0

    @staticmethod
    def get_instance():
        if not BeltManager.instance:
            BeltManager.instance = BeltManager()
        return BeltManager.instance

    def make_belt(self):
        return BeltManager.instance.make_belt()

    def generate_item(self):

        # choose the dbHeroes and create toy:
        rand = random.randrange(4)
        if rand == 0:
            toy = DragonBall(DBHeroes.SANGOKU)
        elif rand == 1:
            toy = DragonBall(DBHeroes.BEJITA)
        elif rand == 2:
            toy = DragonBall(DBHeroes.BEERUS)
        else:
            toy = DragonBall(DBHeroes.KAMESENNIN)

        # choose the wrapping:
        if (random.randrange(2)%2) == 0:
            wrap = Box()
        else:
            wrap = GiftWrap()

        # choose if wrap or toy:
        if (random.randrange(2)%2) == 0:
            return wrap
        else:
            return toy

    def count_work(self, item):
        if isinstance(item, Furniture):
            if not item.is_open and item.toy is not None:
                BeltManager.count = BeltManager.count + 1
