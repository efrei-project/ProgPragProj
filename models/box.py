from .toy import Toy
from .packaging import Packaging


class Box(Packaging):

    def __init__(self):
        super(Box, self).__init__()

    def open(self):
        return super(Box, self).open()
    
    def close(self):
        tmp = super(Box, self).close()
        return tmp

    def insert(self, toy: Toy):
        return super(Box, self).insert(toy)
    
    def remove(self):
        tmp = super(Box, self).remove()
        return tmp
