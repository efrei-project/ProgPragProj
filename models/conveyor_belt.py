from .furniture import Furniture
from .belt_manager import BeltManager


class ConveyorBelt(Furniture):

    def __init__(self, is_busy=False):
        self.is_busy = is_busy
        self.item = None
        self.instance = BeltManager().get_instance()
        super(ConveyorBelt, self).__init__()

    def take(self, index=-1):
        if self.item is not None:
            old_item = self.item
            self.item = None
            self.is_busy = False
            return old_item
        else:
            return None

    def put(self, item):
        if not self.is_busy and item is not None:
            self.is_busy = True
            self.item = item
            return True
        else:
            return False

    def button_in(self):
        if not self.is_busy:
            self.item = self.instance.generate_item()
            self.is_busy = True
            return True
        else:
            return False

    def button_out(self):
        if self.is_busy:
            self.item = None
            self.is_busy = False
            return True
        else:
            return True

    def is_not_full(self):
        return not self.is_busy
