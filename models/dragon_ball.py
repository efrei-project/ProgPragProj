from .figurine import Figurine
from .db_heroes import DBHeroes


class DragonBall(Figurine):

    def __init__(self, _character: DBHeroes):
        self.__character = _character
        print(f'{self.get_character()} is singing -->\n'
              f'CHA-LA HEAD CHA-LA\n'
              f'Nani ga okite mo kibun wa heno-heno kappa\n'
              f'CHA-LA HEAD CHA-LA\n'
              f'Mune ga pachi-pachi suru hodo\n'
              f'Sawagu Genki-Dama --Sparking !')
        self.type_toy = f'{self.__class__.__name__} figurine {self.get_character()}'

    def is_moved(self):
        return "Kamé Kamé Ha!"

    def get_character(self):
        return str(self.__character)
