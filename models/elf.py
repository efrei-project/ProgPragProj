from .toy import Toy
from .packaging import Packaging
from .box import Box
from .furniture import Furniture
from .table import Table


class Elf:

    def __init__(self, nickname: str):
        self._nickname = nickname
        self.item_in_hand = None

    def get_nickname(self):
        return self._nickname

    def set_nickname(self, nickname: str):
        self._nickname = nickname
        return self

    def pack(self, toy: Toy, packaging: Packaging):
        if toy is None:
            return False
        if not packaging.is_open and isinstance(packaging, Box):
            return False
        if packaging.toy is not None:
            return False
        packaging.insert(toy=toy)
        return True

    def unpack(self, packaging: Packaging):
        if packaging.is_open or packaging.toy is None:
            return False
        else:
            packaging.open()
            self.item_in_hand = packaging.toy
            packaging.toy = None
            return True

    def take(self, furniture: Furniture, index=-1):
        self.item_in_hand = furniture.take(index)
        return self.item_in_hand if self.item_in_hand is not None else None

    def put(self, furniture: Furniture, item):
        if furniture is not None and item is not None:
            tmp = furniture.put(item)
            self.item_in_hand = None if tmp else self.item_in_hand
            return True if tmp else False
        else:
            return False

    def look(self, table: Table):
        return table.look()

    def take_from_furniture_to_other(self, origin: Furniture, destination: Furniture, table: Furniture):
        if len(origin.content) > 0:
            if not destination.is_not_full():
                self.desintegrate()
            if self.item_in_hand is not None:
                self.desintegrate()
            self.take(furniture=origin)
            self.put(furniture=destination)
            return True
        else:
            return False

    def is_material_enough_for_gift(self, list_table: [], belt: Furniture):
        list_table.append(self.item_in_hand)
        list_table.append(belt.item)
        package, toy = None, None
        for elem in list_table:
            if isinstance(elem, Packaging):
                package = elem
            if isinstance(elem, Toy):
                toy = elem
        trigger = all((package, toy))
        return True if trigger else False

    def desintegrate(self, maybe_belt: Furniture, maybe_table: Furniture):
        (table, belt) = (maybe_table, maybe_belt) if isinstance(maybe_table, Table) else (maybe_belt, maybe_table)
        belt.button_out()
        self.put(furniture=belt, item=self.item_in_hand)
        belt.button_out()
        if not table.is_not_full():
            self.take(furniture=table)
            self.put(furniture=belt, item=self.item_in_hand)
            belt.button_out()
        return

    def take_all_and_wrap(self, table: Furniture, belt: Furniture):
        if belt.is_busy:
            if isinstance(belt.item, Toy):
                toy = belt.item
            else:
                wrap = belt.item

        for elem in self.look(table=table):
            if isinstance(elem, Toy):
                toy = elem if toy is None else toy
            else:
                wrap = elem if wrap is None else wrap

        self.pack(toy=toy, packaging=wrap)
        if self.item_in_hand is not None:
            if table.is_not_full():
                self.put(furniture=table, item=self.item_in_hand)
        if toy in self.look(table=table):
            self.take(furniture=table, index=self.look(table=table).index(toy))
            if belt.is_busy:
                belt.button_out()
            self.put(furniture=belt, item=toy)
            belt.button_out()
        elif toy == belt.item:
            belt.button_out()
        else:
            self.put(furniture=belt, item=toy)
            belt.button_out()
