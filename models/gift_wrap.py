from .toy import Toy
from .packaging import Packaging


class GiftWrap(Packaging):

    def __init__(self):
        super(GiftWrap, self).__init__()

    def open(self):
        return super(GiftWrap, self).open()

    def close(self):
        return super(GiftWrap, self).close()

    def insert(self, toy: Toy):
        return super(GiftWrap, self).insert(toy)

    def remove(self):
        return super(GiftWrap, self).remove()
