from .toy import Toy


class Packaging:
    def __init__(self, is_open=False):
        self.is_open = is_open
        self.toy = None

    def open(self):
        if self.is_open:
            return False
        else:
            self.is_open = True
            return True

    def close(self):
        if self.is_open:
            self.is_open = False
            return True
        else:
            return False

    def insert(self, toy: Toy):
        if self.is_open and self.toy is None:
            self.toy = toy
            toy.is_moved()
            self.is_open = False
            return True
        else:
            return False

    def remove(self):
        if self.is_open and self.toy is not None:
            self.toy.is_moved()
            self.toy = None
            return True
        else:
            return False
