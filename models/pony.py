from .toy import Toy


class Pony(Toy):

    _nb = 1

    def __init__(self):
        self.id = Pony._nb

        self.type_toy = f'{self.__class__.__name__}#{self.id}'
        print(f'{self.type_toy} \n'
              f'Dou-double poney, j\'fais izi money\n'
              f'D’où tu m\'connais ? Parle moi en billets violets\n'
              f'Dou-double poney, j\'fais izi money')

        Pony._nb = Pony._nb + 1

    def is_moved(self):
        return 'Huuuuuuhu!'
