import socket


def send_message(ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))

    try:
        sock.sendall(message)
        response = sock.recv(1024)
        print(f'Received: { response }')
        sock.close()
    except Exception as e:
        print(f'Exception: { e }')


if __name__ == "__main__":
    ip, port = '10.3.0.68', 3000


