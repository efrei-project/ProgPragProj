from .furniture import Furniture
from.table_manager import TableManager


class Table(Furniture):
    def __init__(self):
        self.content = []
        self.instance = TableManager().get_instance()
        super(Table, self).__init__()

    def take(self, index=-1):
        if len(self.content) == 0:
            return None
        if index >= 10:
            return None
        obj_to_return = self.content.pop(index)
        return obj_to_return

    def put(self, item):
        if self.is_not_full() and item is not None:
            self.content.append(item)
            return True
        else:
            return False

    def look(self):
        return self.content

    def is_not_full(self):
        return True if len(self.content) < 10 else False
