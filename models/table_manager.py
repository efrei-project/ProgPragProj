from .interface_table_manager import InterfaceTableManager


class TableManager(InterfaceTableManager):

    instance = None

    @staticmethod
    def get_instance():
        if not TableManager.instance:
            TableManager.instance = TableManager()
        return TableManager.instance

    def make_table(self):
        return TableManager.instance.table
