import pytest
from .box import Box
from .dragon_ball import DragonBall, DBHeroes


@pytest.fixture
def box():
    box = Box()
    yield box


@pytest.fixture
def toy():
    toy = DragonBall(DBHeroes.SANGOKU)
    yield toy


def test_insert_box(box, toy):
    # test if box open and already toy
    box.is_open = True
    box.toy = toy
    assert not box.insert(toy=toy)

    # test if box closed and already toy
    box.is_open = False
    box.toy = toy
    assert not box.insert(toy=toy)

    # test if box closed and no toy
    box.is_open = False
    box.toy = None
    assert not box.insert(toy=toy)

    # test if box open and no toy
    box.is_open = True
    box.toy = None
    tmp = box.insert(toy=toy)
    assert tmp


def test_remove_box(box, toy):
    # test if box open and already toy
    box.is_open = True
    box.toy = toy
    assert box.remove()

    # test if box closed and already toy
    box.is_open = False
    box.toy = toy
    assert not box.remove()

    # test if box closed and no toy
    box.is_open = False
    box.toy = None
    assert not box.remove()

    # test if box open and no toy
    box.is_open = True
    box.toy = None
    assert not box.remove()


def test_close_box(box):
    # test if box is  closed
    box.is_open = False
    assert not box.close()

    # test if box is open
    box.is_open = True
    assert box.close()


def test_open_box(box):
    # test if box is  closed
    box.is_open = False
    assert box.open()

    # test if box is open
    box.is_open = True
    assert not box.open()
