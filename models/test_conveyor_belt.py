import pytest
from .conveyor_belt import ConveyorBelt
from .dragon_ball import DragonBall, DBHeroes


@pytest.fixture
def belt():
    belt = ConveyorBelt()
    yield belt


@pytest.fixture
def toy():
    toy = DragonBall(DBHeroes.SANGOKU)
    yield toy


def test_take_belt(belt, toy):

    # test take when belt is empty
    assert belt.take() is None

    # test take when belt have 1 item
    belt.put(item=toy)
    assert belt.take() == toy

    # test take at index > nb elem
    for i in range(3):
        belt.content.append(toy)
    assert belt.take(6) is None

    # test take at index > 10
    assert belt.take(25) is None


def test_put_belt(belt, toy):
    # test put nothing
    assert not belt.put(item=None)

    # test put a toy
    assert belt.put(item=toy) is True

    # test put when belt is full
    for i in range(10):
        belt.content.append(toy)
    assert not belt.put(toy)


def test_button_in_belt(belt, toy):

    # test button in when something in belt
    belt.item = toy
    belt.is_busy = True
    assert not belt.button_in()

    # test button in when nothing in belt
    belt.item = None
    belt.is_busy = False
    assert belt.button_in()


def test_button_out_belt(belt, toy):

    # test button out when something in belt
    belt.item = toy
    belt.is_busy = True
    assert belt.button_out()

    # test button out when nothing in belt
    belt.item = None
    belt.is_busy = False
    assert belt.button_out()


def test_is_busy_belt(belt, toy):

    assert not belt.is_busy
    belt.put(toy)
    assert belt.is_busy
