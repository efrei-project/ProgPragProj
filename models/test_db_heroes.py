import pytest
from .db_heroes import DBHeroes


@pytest.fixture
def list_heroes():
    hero_one = DBHeroes.SANGOKU
    hero_two = DBHeroes.BEJITA
    hero_three = DBHeroes.BEERUS
    hero_four = DBHeroes.KAMESENNIN

    yield hero_one, hero_two, hero_three, hero_four


def test_db_heroes(list_heroes):
    a, b, c, d = list_heroes

    assert a == 'SANGOKU'
    assert b == 'BEJITA'
    assert c == 'BEERUS'
    assert d == 'KAMESENNIN'
