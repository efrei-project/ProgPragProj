import pytest
from .dragon_ball import DragonBall, DBHeroes


@pytest.fixture
def hero():
    hero = DragonBall(DBHeroes.SANGOKU)

    yield hero


def test_drabonball(hero):

    assert hero.get_character() == 'SANGOKU'
    assert hero.type_toy == f'{hero.__class__.__name__} figurine {DBHeroes.SANGOKU}'
    assert hero.is_moved() == "Kamé Kamé Ha!"
