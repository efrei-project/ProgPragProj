import pytest
from .elf import Elf
from .dragon_ball import DBHeroes, DragonBall
from .box import Box
from .gift_wrap import GiftWrap
from .table import Table
from .conveyor_belt import ConveyorBelt


@pytest.fixture
def belt():
    belt = ConveyorBelt()
    yield belt


@pytest.fixture
def box():
    box = Box()
    yield box


@pytest.fixture
def wrap():
    wrap = GiftWrap()
    yield wrap


@pytest.fixture
def toy():
    toy = DragonBall(DBHeroes.SANGOKU)
    yield toy


@pytest.fixture
def table():
    table = Table()
    yield table


@pytest.fixture
def elf():
    elf = Elf(nickname='Roger')
    yield elf


def test_nickname_elf(elf):
    assert elf.get_nickname() == 'Roger'
    elf.set_nickname('Steeve')
    assert elf.get_nickname() == 'Steeve'


def test_pack_elf(elf, box, toy):
    # test to pack nothing in closed stuff with something in
    box.is_open = False
    box.toy = toy
    assert not elf.pack(toy=None, packaging=box)

    # test to pack something in closed stuff with something in
    box.is_open = False
    box.toy = toy
    assert not elf.pack(toy=toy, packaging=box)

    # test to pack nothing in open stuff with something in
    box.is_open = True
    box.toy = toy
    assert not elf.pack(toy=None, packaging=box)

    # test to pack something in open stuff with something in
    box.is_open = True
    box.toy = toy
    assert not elf.pack(toy=toy, packaging=box)

    # test to pack nothing in closed stuff with nothing in
    box.is_open = False
    box.toy = None
    assert not elf.pack(toy=None, packaging=box)

    # test to pack something in closed stuff with nothing in
    box.is_open = False
    box.toy = None
    assert not elf.pack(toy=toy, packaging=box)

    # test to pack nothing in open stuff with nothing in
    box.is_open = True
    box.toy = None
    assert not elf.pack(toy=None, packaging=box)

    # test to pack something in open stuff with nothing in
    box.is_open = True
    box.toy = None
    assert elf.pack(toy=toy, packaging=box)


def test_unpack_elf(elf, box, toy):
    # test to unpack an open empty box
    box.is_open = True
    box.toy = None
    assert not elf.unpack(packaging=box)

    # test to unpack a closed empty box
    box.is_open = False
    box.toy = None
    assert not elf.unpack(packaging=box)

    # test to unpack an open filled box
    box.is_open = True
    box.toy = toy
    assert not elf.unpack(packaging=box)

    # test to unpack a closed filled box
    box.is_open = False
    box.toy = toy
    assert elf.unpack(packaging=box)


def test_take_elf(elf, table, toy):
    # test take something from empty table
    table.content = []
    assert elf.take(furniture=table) is None

    # test take something from filled table
    table.content.append(toy)
    assert elf.take(furniture=table) == toy


def test_put_elf(elf, table, toy):
    # test put something to an empty furniture
    table.content = []
    assert elf.put(furniture=table, item=toy)

    # test put nothing to an empty furniture
    table.content = []
    assert not elf.put(furniture=table, item=None)

    # test put something in a few items filled furniture
    table.content = []
    for i in range(5):
        table.put(toy)
    assert elf.put(furniture=table, item=toy)

    # test put nothing in a few items filled furniture
    table.content = []
    for i in range(5):
        table.put(toy)
    assert not elf.put(furniture=table, item=None)

    # test put nothing in a full filled furniture
    table.content = []
    for i in range(10):
        table.put(toy)
    assert not elf.put(furniture=table, item=None)

    # test put something in a full filled furniture
    table.content = []
    for i in range(10):
        table.put(toy)
    assert not elf.put(furniture=table, item=toy)


def test_look_elf(elf, table, toy):
    tmp = []
    for i in range(10):
        table.put(toy)
        tmp.append(toy)
    assert elf.look(table=table) == tmp


def test_is_material_enough_for_gift_elf(elf, table, belt, toy, box):
    box.is_open = True
    box.toy = None

    # test if nothing
    table.content = []
    belt.item = None
    elf.item_in_hand = None
    assert not elf.is_material_enough_for_gift(list_table=elf.look(table=table), belt=belt)

    # test if 2 in table
    table.content = [toy, box]
    belt.item = None
    elf.item_in_hand = None
    assert elf.is_material_enough_for_gift(list_table=elf.look(table=table), belt=belt)

    # test if one in table, other in hand
    table.content = [box]
    belt.item = toy
    elf.item_in_hand = None
    assert elf.is_material_enough_for_gift(list_table=elf.look(table=table), belt=belt)

    # test if one in hand, other in belt
    table.content = []
    belt.item = box
    elf.item_in_hand = toy
    assert elf.is_material_enough_for_gift(list_table=elf.look(table=table), belt=belt)


def test_desintegrate(elf, toy, belt, table):
    # test with no entity full
    belt.item = None
    elf.item_in_hand = None
    table.content = []
    elf.desintegrate(maybe_table=table, maybe_belt=belt)
    assert belt.item is None
    assert elf.item_in_hand is None
    assert len(table.content) <= 9

    # test with furniture empty and hand full
    belt.item = None
    elf.item_in_hand = toy
    table.content = []
    elf.desintegrate(maybe_table=table, maybe_belt=belt)
    assert belt.item is None
    assert elf.item_in_hand is None
    assert len(table.content) <= 9

    # test with belt full + hand
    belt.item = toy
    elf.item_in_hand = toy
    table.content = []
    elf.desintegrate(maybe_table=table, maybe_belt=belt)
    assert belt.item is None
    assert elf.item_in_hand is None
    assert len(table.content) <= 9

    # test with table full + hand
    belt.item = None
    elf.item_in_hand = toy
    table.content = [toy, toy, toy, toy, toy, toy, toy, toy, toy, toy, ]
    elf.desintegrate(maybe_table=table, maybe_belt=belt)
    assert belt.item is None
    assert elf.item_in_hand is None
    assert len(table.content) <= 9

    # test with all full
    belt.item = None
    elf.item_in_hand = None
    table.content = [toy, toy, toy, toy, toy, toy, toy, toy, toy, toy, ]
    elf.desintegrate(maybe_table=table, maybe_belt=belt)
    assert belt.item is None
    assert elf.item_in_hand is None
    assert len(table.content) <= 9

    # test with invert belt and table (just belt full)
    belt.item = None
    elf.item_in_hand = None
    table.content = []
    elf.desintegrate(maybe_table=belt, maybe_belt=table)
    assert belt.item is None
    assert elf.item_in_hand is None
    assert len(table.content) <= 9


def test_take_all_and_wrap_elf(toy, box, elf, table, belt):
    # test 