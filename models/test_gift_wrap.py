import pytest
from .gift_wrap import GiftWrap
from .dragon_ball import DragonBall, DBHeroes


@pytest.fixture
def giftwrap():
    giftwrap = GiftWrap()
    yield giftwrap


@pytest.fixture
def toy():
    toy = DragonBall(DBHeroes.SANGOKU)
    yield toy


def test_insert_giftwrap(giftwrap, toy):
    # test if giftwrap open and already toy
    giftwrap.is_open = True
    giftwrap.toy = toy
    assert not giftwrap.insert(toy=toy)

    # test if giftwrap closed and already toy
    giftwrap.is_open = False
    giftwrap.toy = toy
    assert not giftwrap.insert(toy=toy)

    # test if giftwrap closed and no toy
    giftwrap.is_open = False
    giftwrap.toy = None
    assert not giftwrap.insert(toy=toy)

    # test if giftwrap open and no toy
    giftwrap.is_open = True
    giftwrap.toy = None
    assert giftwrap.insert(toy=toy)


def test_remove_giftwrap(giftwrap, toy):
    # test if giftwrap open and already toy
    giftwrap.is_open = True
    giftwrap.toy = toy
    assert giftwrap.remove()

    # test if giftwrap closed and already toy
    giftwrap.is_open = False
    giftwrap.toy = toy
    assert not giftwrap.remove()

    # test if giftwrap closed and no toy
    giftwrap.is_open = False
    giftwrap.toy = None
    assert not giftwrap.remove()

    # test if giftwrap open and no toy
    giftwrap.is_open = True
    giftwrap.toy = None
    assert not giftwrap.remove()


def test_close_giftwrap(giftwrap):
    # test if giftwrap is already closed
    giftwrap.is_open = True
    assert giftwrap.close()

    # test if giftwrap is open
    giftwrap.is_open = False
    assert not giftwrap.close()


def test_open_giftwrap(giftwrap):
    # test if giftwrap is already closed
    giftwrap.is_open = True
    assert not giftwrap.open()

    # test if giftwrap is open
    giftwrap.is_open = False
    assert giftwrap.open()
