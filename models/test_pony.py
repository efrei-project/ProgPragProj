from .pony import Pony
import pytest


@pytest.fixture
def pony():
    pony = Pony()
    yield pony


def test_pony(pony):
    # test if _nb change from 1 to 2
    assert Pony._nb == 2

    # test type / id / is_moved()
    assert pony.type_toy == f'{pony.__class__.__name__}#{pony.id}'
    assert pony.id == 1
    assert pony.is_moved() == 'Huuuuuuhu!'

    # test type / id / is_moved()
    pony = Pony()
    assert pony.type_toy == f'{pony.__class__.__name__}#{pony.id}'
    assert pony.id == 2
    assert pony.is_moved() == 'Huuuuuuhu!'

    assert Pony._nb == 3
