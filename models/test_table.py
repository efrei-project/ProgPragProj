import pytest
from .table import Table
from .dragon_ball import DragonBall, DBHeroes


@pytest.fixture
def table():
    table = Table()
    yield table


@pytest.fixture
def toy():
    toy = DragonBall(DBHeroes.SANGOKU)
    yield toy


def test_take_table(table, toy):

    # test take when table is empty
    assert table.take() is None

    # test take when table have 1 item
    table.content.append(toy)
    assert table.take() == toy

    # test take at index when table is full item
    for i in range(10):
        if i == 5:
            table.content.append(toy)
        else:
            table.content.append(None)
    tmp = table.look()
    assert table.take(index=5) == toy

    # test take at index > nb elem
    for i in range(3):
        table.content.append(toy)
    assert table.take(6) is None

    # test take at index > 10
    assert table.take(25) is None


def test_put_table(table, toy):
    # test put nothing
    assert not table.put(item=None)

    # test put a toy
    assert table.put(item=toy) is True

    # test put when table is full
    for i in range(10):
        table.content.append(toy)
    assert not table.put(toy)


def test_look_table(table, toy):
    tmp = []
    for i in range(10):
        tmp.append(toy)
    for i in range(10):
        table.content.append(toy)
    assert table.look() == tmp
