import pytest
from .toy import Toy


@pytest.fixture
def toy():
    toy = Toy()
    yield toy


def test_toy(toy):

    toy.type_toy = 'toy1'
    assert toy.type_toy == 'toy1'
